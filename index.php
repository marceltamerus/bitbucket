<?php

require 'loader.php';

$availableFilters = [
    'In Code Review' => '',
    'Needs Work' => 'alert-danger',
    'To Test' => 'alert-info',
    'To Merge' => 'alert-success'
];

$currentFilter = isset($_REQUEST['filter']) ? $_REQUEST['filter'] : '';

function formatFilters()
{
    global $availableFilters, $currentFilter;

    $return = [];

    $return[] = '<a class="btn btn-' . ($currentFilter === '' ? 'primary' : 'dark') . ' btn-sm" href=".">Alles</a>';

    foreach (array_keys($availableFilters) as $filter) {
        $return[] = '<a class="btn btn-' . ($currentFilter === $filter ? 'primary' : 'dark') . ' btn-sm" href="?filter=' . $filter . '">' . $filter . '</a>';
    }

    return implode('', $return);
}

function formatRows()
{
    global $availableFilters, $currentFilter;

    $count = 0;
    $dbPullRequest = new PullRequest();
    $dbPullRequestParticipant = new PullRequestParticipant();
    $return = [];

    foreach ($dbPullRequest->getAll() as $pullRequest) {
        if ($currentFilter !== '' && $pullRequest["jira_status"] !== $currentFilter) {
            continue;
        }

        $reviewState = '';

        foreach ($dbPullRequestParticipant->getDetails($pullRequest["bitbucket_id"], $pullRequest["repository"]) as $participant) {
            $reviewState .= '<div class="text-nowrap">' . $participant["user_display_name"] . ($participant["approved"] ? ' <i class="far fa-thumbs-up"></i>' : '') . '</div>';
        }

        $class = 'alert-warning';

        if (isset($availableFilters[$pullRequest["jira_status"]])) {
            $class = $availableFilters[$pullRequest["jira_status"]];
        }

        $cells = [];

        $cells[] = ++$count;
        $cells[] = $pullRequest["jira_status"];
        $cells[] = $pullRequest["name"];
        $cells[] = '<a href="' . $pullRequest["href"] . '" target="_blank">' . $pullRequest["title"] . '</a>';
        $cells[] = $pullRequest["repository"];
        $cells[] = '<a href="' . 'https://webfant.atlassian.net/browse/' . $pullRequest["jira_key"] . '" target="_blank">' . $pullRequest["jira_key"] . '</a>';
        $cells[] = $reviewState;

        $return[] = '<tr' . ($class ? ' class="' . $class . '"' : '') . '><td>' . implode('</td><td>', $cells) . '</td></tr>';
    }

    return implode('', $return);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="refresh" content="300">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Code reviews</title>
    <style type="text/css">
        body {padding-top: 48px}
    </style>
</head>
<body>
<nav class="navbar fixed-top bg-dark text-center text-light">
    <div class="container-fluid">
        <div class="col-12 ">
            <?= formatFilters() ?>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <div class="col-12">
        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Status</th>
                <th>Name</th>
                <th>Title</th>
                <th>Repository</th>
                <th>Jira</th>
                <th>Reviewers</th>
            </tr>
            </thead>
            <tbody><?= formatRows() ?></tbody>
        </table>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/684de7e88e.js" crossorigin="anonymous"></script>
</body>
</html>
