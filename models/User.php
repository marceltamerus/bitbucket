<?php

/**
 * Class User
 */
class User
{

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->pdo = new PDO('sqlite:' . DB_PATH);
    }

    /**
     * @return mixed
     */
    public function get()
    {
        $query = $this->pdo->prepare("SELECT * FROM users");

        $query->execute();

        return $query->fetchAll();
    }

    /**
     * @param $code_name
     *
     * @return mixed
     */
    public function getByCodeName($code_name)
    {
        $query = $this->pdo->prepare("SELECT * FROM users WHERE code_name = :code_name");

        $query->execute([
            ':code_name' => $code_name,
        ]);

        return $query->fetch();
    }
}
