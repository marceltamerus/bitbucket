<?php

use JiraRestApi\Issue\IssueService;
use JiraRestApi\JiraException;

/**
 * Class Jira
 */
class Jira
{

    /**
     * @param $ticket
     *
     * @return array|string[]
     * @throws JsonMapper_Exception
     */
    public function getTicket($ticket)
    {
        $title = explode(' ', $ticket)[0];
        $title = str_replace('feature/', '', $title);
        $title = str_replace('Feature/', '', $title);
        $title = str_replace('hotfix/', '', $title);
        $title = str_replace('Hotfix/', '', $title);

        $fixVersion = '';
        $key = '';
        $status = '';
        $summary = '';

        try {
            $issueService = new IssueService();

            $queryParam = [
                'fields' => [  // default: '*all'
                               'summary',
                               'comment',
                ],
                'expand' => [
                    'renderedFields',
                    'names',
                    'schema',
                    'transitions',
                    'operations',
                    'editmeta',
                    'changelog',
                ],
            ];

            $issue = $issueService->get($title, $queryParam);

            $key = $issue->key;
            $summary = $issue->fields->summary;

            $histories = $issue->changelog->histories;

            foreach ($histories as $history) {
                foreach ($history->items as $item) {
                    if (!property_exists($item, 'fieldId')) {
                        continue;
                    }

                    if ($item->fieldId === 'fixVersions' && $fixVersion === '') {
                        $fixVersion = $item->toString;
                    }

                    if ($item->fieldId === 'status' && $status === '') {
                        $status = $item->toString;
                    }

                    if ($fixVersion !== '' && $status !== '') {
                        break(2);
                    }
                }
            }

        } catch (JiraException $e) {
            print($e->getMessage());
        }

        return [
            'fix_version' => $fixVersion,
            'key' => $key,
            'status' => $status,
            'summary' => $summary,
        ];
    }
}
