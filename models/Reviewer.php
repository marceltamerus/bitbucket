<?php

/**
 * Class Reviewer
 */
class Reviewer
{

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * Reviewer constructor.
     */
    public function __construct()
    {
        $this->pdo = new PDO('sqlite:' . DB_PATH);
    }

    /**
     * @param $reviewer_id
     *
     * @return array
     */
    public function getByReviewer($reviewer_id)
    {
        $query = $this->pdo->prepare("SELECT *  FROM users WHERE id IN (SELECT user_id FROM reviewers WHERE reviewer_id = :reviewer_id)");

        $query->execute([
            ':reviewer_id' => $reviewer_id,
        ]);

        return $query->fetchAll();
    }

    /**
     * @param $user_id
     *
     * @return array
     */
    public function getByUser($user_id)
    {
        $query = $this->pdo->prepare("SELECT *  FROM users WHERE id IN (SELECT reviewer_id FROM reviewers WHERE user_id = :user_id)");

        $query->execute([
            ':user_id' => $user_id,
        ]);

        return $query->fetchAll();
    }
}
