<?php

/**
 * Class PullRequest
 */
class PullRequest
{

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * PullRequest constructor.
     */
    public function __construct()
    {
        $this->pdo = new PDO('sqlite:' . DB_PATH);
    }

    /**
     * @param $bitbucket_id
     * @param $repository
     */
    public function delete($bitbucket_id, $repository)
    {
        $query = $this->pdo->prepare("DELETE FROM pull_requests WHERE bitbucket_id = :bitbucket_id AND repository = :repository");

        $query->execute([
            ':bitbucket_id' => $bitbucket_id,
            ':repository' => $repository,
        ]);
    }

    /**
     * @param $bitbucket_id
     * @param $repository
     *
     * @return mixed
     */
    public function get($bitbucket_id, $repository)
    {
        $query = $this->pdo->prepare("SELECT * FROM pull_requests WHERE bitbucket_id = :bitbucket_id AND repository = :repository");

        $query->execute([
            ':bitbucket_id' => $bitbucket_id,
            ':repository' => $repository,
        ]);

        return $query->fetchObject();
    }

    /**
     * @return array
     */
    public function getbyCronUpdate()
    {
        return $this->pdo->query("SELECT * FROM pull_requests WHERE cron_update IS NULL")->fetchAll();
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $query = $this->pdo->prepare("SELECT u.name, pr.* FROM pull_requests AS pr JOIN users AS u on u.id = pr.user_id ORDER BY u.name ASC, pr.repository ASC, updated_on DESC");

        $query->execute();

        return $query->fetchAll();
    }

    /**
     * @param $user_id
     * @param $bitbucket_id
     * @param $title
     * @param $href
     * @param $repository
     * @param $updated_on
     * @param $comment_count
     * @param $jira_fix_version
     * @param $jira_status
     * @param $jira_key
     * @param $jira_summary
     */
    public function insert($user_id, $bitbucket_id, $title, $href, $repository, $updated_on, $comment_count, $jira_fix_version, $jira_status, $jira_key, $jira_summary)
    {
        $query = $this->pdo->prepare("
                INSERT INTO pull_requests (
                    user_id,
                    bitbucket_id,
                    title,
                    href,
                    repository,
                    updated_on,
                    comment_count,
                    cron_update,
                    jira_fix_version,
                    jira_status,
                    jira_key,
                    jira_summary
                ) VALUES (
                    :user_id,
                    :bitbucket_id,
                    :title,
                    :href,
                    :repository,
                    :updated_on,
                    :comment_count,
                    1,
                    :jira_fix_version,
                    :jira_status,
                    :jira_key,
                    :jira_summary
                )
            ");

        $query->execute([
            ':user_id' => $user_id,
            ':bitbucket_id' => $bitbucket_id,
            ':title' => $title,
            ':href' => $href,
            ':repository' => $repository,
            ':updated_on' => $updated_on,
            ':comment_count' => $comment_count,
            ':jira_fix_version' => $jira_fix_version,
            ':jira_status' => $jira_status,
            ':jira_key' => $jira_key,
            ':jira_summary' => $jira_summary,
        ]);
    }

    /**
     * @param $user_id
     * @param $bitbucket_id
     * @param $title
     * @param $href
     * @param $repository
     * @param $updated_on
     * @param $comment_count
     * @param $jira_fix_version
     * @param $jira_status
     * @param $jira_key
     * @param $jira_summary
     */
    public function update($user_id, $bitbucket_id, $title, $href, $repository, $updated_on, $comment_count, $jira_fix_version, $jira_status, $jira_key, $jira_summary)
    {
        $query = $this->pdo->prepare("
                    UPDATE pull_requests SET
                        user_id = :user_id,
                        title = :title,
                        href = :href,
                        updated_on = :updated_on,
                        comment_count = :comment_count,
                        cron_update = 1,
                        jira_fix_version = :jira_fix_version,
                        jira_status = :jira_status,
                        jira_key = :jira_key,
                        jira_summary = :jira_summary
                    WHERE bitbucket_id = :bitbucket_id AND repository = :repository
                ");

        $query->execute([
            ':user_id' => $user_id,
            ':bitbucket_id' => $bitbucket_id,
            ':title' => $title,
            ':href' => $href,
            ':repository' => $repository,
            ':updated_on' => $updated_on,
            ':comment_count' => $comment_count,
            ':jira_fix_version' => $jira_fix_version,
            ':jira_status' => $jira_status,
            ':jira_key' => $jira_key,
            ':jira_summary' => $jira_summary,
        ]);
    }

    /**
     * @param $state
     * @param $bitbucket_id
     * @param $repository
     */
    public function updateState($state, $bitbucket_id, $repository)
    {
        $query = $this->pdo->prepare("
                    UPDATE pull_requests SET
                        state = :state
                    WHERE bitbucket_id = :bitbucket_id AND repository = :repository
                ");

        $query->execute([
            ':state' => $state,
            ':bitbucket_id' => $bitbucket_id,
            ':repository' => $repository,
        ]);
    }

    /**
     *
     */
    public function resetCronUpdate()
    {
        $this->pdo->query("UPDATE pull_requests SET cron_update = NULL, state = NULL");
    }
}
