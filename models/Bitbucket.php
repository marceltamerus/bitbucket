<?php

use Bitbucket\Client;

/**
 * Class Bitbucket
 */
class Bitbucket
{

    /**
     * @var Client
     */
    private $bitbucket;

    /**
     * Bitbucket constructor.
     */
    public function __construct($username, $password)
    {
        $this->bitbucket = new Client();
        $this->bitbucket->authenticate(Client::AUTH_HTTP_PASSWORD, $username, $password);
    }

    /**
     * @return mixed
     * @throws \Http\Client\Exception
     */
    public function getCurrentUser()
    {
        $currentUser = $this->bitbucket->currentUser()->show();

        return json_decode(json_encode($currentUser));
    }

    /**
     * @param $active
     *
     * @return mixed
     * @throws \Http\Client\Exception
     */
    public function getPullRequests($active)
    {
        $pullRequests = $this->bitbucket->pullRequests();

        return json_decode(json_encode($pullRequests->list($active, ['state' => 'OPEN'])));
    }

    /**
     * @param $username
     * @param $repository
     * @param $pullRequestId
     *
     * @return mixed
     * @throws \Http\Client\Exception
     */
    public function getPullRequestDetails($username, $repository, $pullRequestId)
    {
        $pullRequestDetails = $this->bitbucket->repositories()->workspaces('webfant')->pullRequests($repository)->show($pullRequestId);

        return json_decode(json_encode($pullRequestDetails));
    }

    /**
     * @return \Bitbucket\Api\Repositories
     */
    public function getRepositories()
    {
        return $this->bitbucket->repositories();
    }
}
