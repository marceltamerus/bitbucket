<?php

/**
 * Class PullRequestParticipant
 */
class PullRequestParticipant
{

    /**
     * @var PDO
     */
    private $pdo;

    /**
     * PullRequest constructor.
     */
    public function __construct()
    {
        $this->pdo = new PDO('sqlite:' . DB_PATH);
    }

    /**
     * @param $bitbucket_id
     * @param $repository
     */
    public function delete($bitbucket_id, $repository)
    {
        $query = $this->pdo->prepare("DELETE FROM pull_request_participants WHERE bitbucket_id = :bitbucket_id AND repository = :repository");

        $query->execute([
            ':bitbucket_id' => $bitbucket_id,
            ':repository' => $repository,
        ]);
    }

    /**
     * @param $bitbucket_id
     * @param $repository
     * @param $user_display_name
     *
     * @return mixed
     */
    public function get($bitbucket_id, $repository, $user_display_name)
    {
        $query = $this->pdo->prepare("SELECT * FROM pull_request_participants WHERE bitbucket_id = :bitbucket_id AND repository = :repository AND user_display_name = :user_display_name");

        $query->execute([
            ':bitbucket_id' => $bitbucket_id,
            ':repository' => $repository,
            ':user_display_name' => $user_display_name,
        ]);

        return $query->fetchObject();
    }

    /**
     * @param $bitbucket_id
     * @param $repository
     *
     * @return mixed
     */
    public function getDetails($bitbucket_id, $repository)
    {
        $query = $this->pdo->prepare("SELECT * FROM pull_request_participants WHERE bitbucket_id = :bitbucket_id AND repository = :repository");

        $query->execute([
            ':bitbucket_id' => $bitbucket_id,
            ':repository' => $repository,
        ]);

        return $query->fetchAll();
    }

    /**
     * @param $user_display_name
     * @param $approved
     * @param $bitbucket_id
     * @param $repository
     */
    public function insert($user_display_name, $approved, $bitbucket_id, $repository)
    {
        $query = $this->pdo->prepare("
                        INSERT INTO pull_request_participants (
                            user_display_name,
                            approved,
                            bitbucket_id,
                            repository
                        ) VALUES (
                            :user_display_name,
                            :approved,
                            :bitbucket_id,
                            :repository
                        )
                    ");

        $query->execute([
            ':user_display_name' => $user_display_name,
            ':approved' => $approved,
            ':bitbucket_id' => $bitbucket_id,
            ':repository' => $repository,
        ]);
    }

    /**
     * @param $user_display_name
     * @param $approved
     * @param $bitbucket_id
     * @param $repository
     */
    public function update($user_display_name, $approved, $bitbucket_id, $repository)
    {
        $query = $this->pdo->prepare("UPDATE pull_request_participants SET approved = :approved WHERE bitbucket_id = :bitbucket_id AND repository = :repository AND user_display_name = :user_display_name");

        $query->execute([
            ':user_display_name' => $user_display_name,
            ':approved' => $approved,
            ':bitbucket_id' => $bitbucket_id,
            ':repository' => $repository,
        ]);
    }
}
