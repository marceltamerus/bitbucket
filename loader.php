<?php

require 'vendor/autoload.php';
require 'models/Bitbucket.php';
require 'models/Jira.php';
require 'models/PullRequest.php';
require 'models/PullRequestParticipant.php';
require 'models/Reviewer.php';
require 'models/User.php';

const DB_PATH = './data/bitbucket.db';
