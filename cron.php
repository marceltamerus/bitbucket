<?php

chdir(dirname(__FILE__));

require 'loader.php';

$dbPullRequest = new PullRequest();
$dbPullRequestParticipant = new PullRequestParticipant();
$dbUser = new User();

$dbUsers = $dbUser->get();
$users = [];
foreach ($dbUsers as $dbUser) {
    $users[$dbUser["code_name"]] = $dbUser;
}

$dbPullRequest->resetCronUpdate();

$bitbucket = new Bitbucket($users["marceltamerus"]["code_name"], $users["marceltamerus"]["password"]);
$jira = new Jira();

foreach ($users as $user) {
    try {
        $pullRequests = $bitbucket->getPullRequests($user["code_name"]);
    } catch (\Http\Client\Exception $e) {
        continue;
    }

    if (count($pullRequests->values) > 0) {
        foreach ($pullRequests->values as $pullRequest) {
            $currentPullRequest = $dbPullRequest->get($pullRequest->id, $pullRequest->destination->repository->name);

            $jiraTicket = $jira->getTicket($pullRequest->title);

            if ($currentPullRequest) {
                $dbPullRequest->update($user["id"], $pullRequest->id, $pullRequest->title, $pullRequest->links->html->href, $pullRequest->destination->repository->name, $pullRequest->updated_on, $pullRequest->comment_count, $jiraTicket['fix_version'], $jiraTicket['status'], $jiraTicket['key'], $jiraTicket['summary']);
            } else {
                $dbPullRequest->insert($user["id"], $pullRequest->id, $pullRequest->title, $pullRequest->links->html->href, $pullRequest->destination->repository->name, $pullRequest->updated_on, $pullRequest->comment_count, $jiraTicket['fix_version'], $jiraTicket['status'], $jiraTicket['key'], $jiraTicket['summary']);
            }

            $values = explode('/', $pullRequest->source->repository->full_name);

            try {
                $pullRequestDetails = $bitbucket->getPullRequestDetails($values[0], $values[1], $pullRequest->id);
            } catch (\Http\Client\Exception $e) {
                continue;
            }

            foreach ($pullRequestDetails->participants as $participant) {
                if ($participant->state === 'changes_requested') {
                    $dbPullRequest->updateState('Wijzig', $pullRequest->id, $pullRequest->destination->repository->name);
                }

                if ($participant->role === 'REVIEWER') {
                    $currentPullRequestParticipant = $dbPullRequestParticipant->get($pullRequest->id, $pullRequest->destination->repository->name, explode(' ', $participant->user->display_name)[0]);

                    if ($currentPullRequestParticipant) {
                        $dbPullRequestParticipant->update(explode(' ', $participant->user->display_name)[0], $participant->approved, $pullRequest->id, $pullRequest->destination->repository->name);
                    } else {
                        $dbPullRequestParticipant->insert(explode(' ', $participant->user->display_name)[0], $participant->approved, $pullRequest->id, $pullRequest->destination->repository->name);
                    }
                }
            }
        }
    }
}

$pullRequests = $dbPullRequest->getbyCronUpdate();

foreach ($pullRequests as $pullRequest) {
    $dbPullRequestParticipant->delete($pullRequest["bitbucket_id"], $pullRequest["repository"]);
    $dbPullRequest->delete($pullRequest["bitbucket_id"], $pullRequest["repository"]);
}
