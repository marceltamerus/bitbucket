<?php

chdir(dirname(__FILE__));

require 'loader.php';

$dbUser = new User();

$dbUsers = $dbUser->get();
$users = [];
foreach ($dbUsers as $dbUser) {
    $users[$dbUser["code_name"]] = $dbUser;
}

$bitbucket = new Bitbucket($users["marceltamerus"]["code_name"], $users["marceltamerus"]["password"]);

$repositories = [
    'axces-m2',
    'babyrentals',
    'baxrecreatieshop-m2',
    'burton',
    'delekkerstekaas',
    'endomed',
    'halfords-m2',
    'jr-sportpromotions',
    'm2saas',
    'magixbuttons',
    'noah',
    'ondermode-m2',
    'plc2day',
    'skiwebshop-m2',
    'vanasten-m2',
    'zonnerij-m2',
];

$all = [];

foreach ($repositories as $repository) {
    $composer = $bitbucket
        ->getRepositories()
        ->workspaces('webfant')
        ->src($repository)
        ->download('master', '/composer.json');

    $contents = $composer->read($composer->getSize());

    foreach ((array)json_decode($contents)->require as $name => $version) {
        $all[$name][$repository] = $version;
    }
}

ksort($all);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <title>Composer modules</title>
    <style>
        * {
            font-size: .95rem;
        }
        thead {
            background-color: rgba(255,255,255,0.75);
            backdrop-filter: blur(5px);
            position: sticky;
            top: 0;
        }
        td, th {
            white-space: nowrap;
        }
        th.vertical {
            writing-mode: vertical-lr;
            transform: rotate(180deg);
        }
    </style>
</head>
<body>
<table class="table table-bordered table-hover table-sm table-striped">

    <thead>
    <tr>
        <th>Module</th>
        <th>State</th>
        <?php
        foreach ($repositories as $item) {
            echo '<th class="vertical"><a href="https://bitbucket.org/webfant/' . $item . '" target="blank">' . $item . '</a></th>';
        }
        ?>
    </tr>
    </thead>

    <tbody>
    <?php
    foreach ($all as $module => $versions) {
        echo '<tr>';
        echo '<td><a href="https://webfant.atlassian.net/wiki/search?text=' . $module . '" target="blank">' . $module . '</a></td>';

        $healthy = true;
        $compare = null;
        foreach ($repositories as $repository) {
            if (isset($versions[$repository])) {
                if (is_null($compare)) {
                    $compare = $versions[$repository];
                    continue;
                }
                if ($versions[$repository] !== $compare) {
                    $healthy = false;
                    break;
                }
            }
        }
        if ($healthy) {
            echo '<td class="table-success"></td>';
        } else {
            echo '<td class="table-danger"></td>';
        }


        foreach ($repositories as $repository) {
            echo '<td>';
            if (isset($versions[$repository])) {
                echo $versions[$repository];
            }
            echo '</td>';
        }
        echo '</tr>';
    }
    ?>
    </tbody>
</table>
<!---->
<!--<div class="container-fluid">-->
<!--    <div class="col-12">-->
<!--        <table class="table">-->
<!--            <thead>-->
<!--            <tr>-->
<!--                <th>Module</th>-->
<!--                <th>Repository</th>-->
<!--                <th>Version</th>-->
<!--            </tr>-->
<!--            </thead>-->
<!--            <tbody>-->
<!--            --><?php
//            foreach ($all as $module => $versions) {
//                $first = true;
//                foreach ($versions as $repository => $version) {
//                    echo '<tr>';
//
//                    if ($first) {
//                        echo '<td rowspan="' . count($versions) . '">';
//                        echo '<a href="https://webfant.atlassian.net/wiki/search?text=' . $module . '" target="blank">' . $module . '</a>';
//                        echo '</td>';
//                    }
//
//                    echo '<td>';
//                    echo '<a href="https://bitbucket.org/webfant/' . $repository . '" target="blank">' . $repository . '</a>';
//                    echo '</td>';
//
//                    echo '<td>';
//                    echo $version;
//                    echo '</td>';
//
//                    echo '</tr>';
//
//                    $first = false;
//                }
//            }
//            ?>
<!--            </tbody>-->
<!--        </table>-->
<!--    </div>-->
<!--</div>-->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/684de7e88e.js" crossorigin="anonymous"></script>
</body>
</html>
